﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace tictactoe_android
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Multi_Player : ContentPage
    {
        private readonly List<List<int>> _allBtnPosition = new List<List<int>>();
        private readonly List<List<Button>> _allBtnObject = new List<List<Button>>();
        public Multi_Player()
        {
            InitializeComponent();

            var btnFirstRow = new List<Button>();
            btnFirstRow.Add(Multi11);
            btnFirstRow.Add(Multi12);
            btnFirstRow.Add(Multi13);

            var btnSecondRow = new List<Button>();
            btnSecondRow.Add(Multi21);
            btnSecondRow.Add(Multi22);
            btnSecondRow.Add(Multi23);

            var btnThirdRow = new List<Button>();
            btnThirdRow.Add(Multi31);
            btnThirdRow.Add(Multi32);
            btnThirdRow.Add(Multi33);

            _allBtnObject.Add(btnFirstRow);
            _allBtnObject.Add(btnSecondRow);
            _allBtnObject.Add(btnThirdRow);


            _allBtnPosition.Add(new List<int> { 2, 2, 2 });
            _allBtnPosition.Add(new List<int> { 2, 2, 2 });
            _allBtnPosition.Add(new List<int> { 2, 2, 2 });
        }

        public void Back_Multi_Clicked(object sender, EventArgs args)
        {
            App.Current.MainPage = new MainPage();
            Console.WriteLine("Back Clicked");

        }

        public bool Who_Won()
        {
            var pos1 = new List<int>();
            foreach (var i in _allBtnPosition)
            {
                pos1.Add(i[0]);
            }

            var pos2 = new List<int>();
            foreach (var i in _allBtnPosition)
            {
                pos2.Add(i[1]);
            }

            var pos3 = new List<int>();
            foreach (var i in _allBtnPosition)
            {
                pos3.Add(i[2]);
            }

            List<int> pos4 = _allBtnPosition[0];
            List<int> pos5 = _allBtnPosition[1];
            List<int> pos6 = _allBtnPosition[2];
            List<int> pos7 = new List<int>();
            List<int> pos8 = new List<int>();

            int i_ = 0;
            int j_ = 2;
            foreach (var i in _allBtnPosition)
            {
                pos7.Add(i[i_]);
                pos8.Add(i[j_]);
                i_++;
                j_--;
            }

            var allPos = new List<List<int>>();
            allPos.Add(pos1);
            allPos.Add(pos2);
            allPos.Add(pos3);
            allPos.Add(pos4);
            allPos.Add(pos5);
            allPos.Add(pos6);
            allPos.Add(pos7);
            allPos.Add(pos8);

            bool hasWon = false;
            foreach (var p in allPos)
            {
                int init = p[0];
                if (init != 2)
                {
                    var initLst = new List<int>();
                    foreach (var i in p)
                    {
                        if (i == init)
                        {
                            initLst.Add(i);
                        }
                    }
                    if (initLst.Count == 3)
                    {
                        hasWon = true;
                        break;
                    }
                }
            }

            return hasWon;
        }

        private int _turn = 0;
        private bool _gameEnd = false;

        public void Multi_Each_Btn_Clicked(object sender, EventArgs args)
        {
            if (_gameEnd != true)
            {
                var currentBtn = sender as Button;

                var columnStack = (StackLayout)currentBtn.Parent;
                int iColumn = 0;
                foreach (var btn in columnStack.Children)
                {
                    if (btn.Id == currentBtn.Id) { break; }
                    iColumn++;
                }

                var rowStack = (StackLayout)columnStack.Parent;
                int iRow = 0;

                foreach (var stack in rowStack.Children)
                {
                    if (stack.Id == columnStack.Id) { break; }
                    iRow++;
                }

                if (_allBtnPosition[iRow][iColumn] == 2)
                {
                    _allBtnPosition[iRow][iColumn] = _turn;

                    currentBtn.BackgroundColor = Color.Transparent;
                    currentBtn.FontSize = 30;
                    string currentStr = "";
                    if (_turn == 0)
                    {
                        currentBtn.Text = "X";
                        _turn = 1;
                        currentStr = "O turn";
                    }
                    else
                    {
                        currentBtn.Text = "O";
                        _turn = 0;
                        currentStr = "X turn";
                    }
                    bool whoWon = Who_Won();
                    if (whoWon)
                    {
                        DisplayMultiGameStatus.Text = "You Won!";
                        _gameEnd = true;
                        if (_turn == 1) { currentStr = "X Won!"; } else { currentStr = "O Won!"; }
                    }
                    DisplayMultiGameStatus.Text = currentStr;
                }
            }
        }

        public void Multi_Reset_Clicked(object sender, EventArgs args)
        {
            App.Current.MainPage = new Multi_Player();

        }


    }
}