﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace tictactoe_android
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class single_player : ContentPage
{       private readonly List<List<int>> _allBtnPosition = new List<List<int>>();
        private readonly List<List<Button>> _allBtnObject = new List<List<Button>>();
        public single_player()
        {
            InitializeComponent();

            var btnFirstRow = new List<Button>();
            btnFirstRow.Add(Single11);
            btnFirstRow.Add(Single12);
            btnFirstRow.Add(Single13);

            var btnSecondRow = new List<Button>();
            btnSecondRow.Add(Single21);
            btnSecondRow.Add(Single22);
            btnSecondRow.Add(Single23);

            var btnThirdRow = new List<Button>();
            btnThirdRow.Add(Single31);
            btnThirdRow.Add(Single32);
            btnThirdRow.Add(Single33);

            _allBtnObject.Add(btnFirstRow);
            _allBtnObject.Add(btnSecondRow);
            _allBtnObject.Add(btnThirdRow);


            _allBtnPosition.Add(new List<int> { 2, 2, 2 });
            _allBtnPosition.Add(new List<int> { 2, 2, 2 });
            _allBtnPosition.Add(new List<int> { 2, 2, 2 });
        }

        private bool _gameEnd = false;

        public void Back_Single_Clicked(object sender, EventArgs args)
        {
            App.Current.MainPage = new MainPage();
        }

        public List<int> Choose_move()
        {
            var unchoosedList = new List<List<int>>();

            foreach (var i in _allBtnPosition)
            {
                foreach(var j in i)
                {
                    if(j == 2)
                    {
                        var d = new List<int>();
                        d.Add(_allBtnPosition.IndexOf(i));
                        d.Add(i.IndexOf(j));
                        unchoosedList.Add(d);
                    }
                }
            }
            if(unchoosedList.Any())
            {
                var random = new Random();
                int index = random.Next(unchoosedList.Count);
                return unchoosedList[index];
            }

            return new List<int>{10, 10};
        }

        public bool Who_Won()
        {
            var pos1 = new List<int>();
            foreach(var i in _allBtnPosition)
            {
                pos1.Add(i[0]);
            }

            var pos2 = new List<int>();
            foreach (var i in _allBtnPosition)
            {
                pos2.Add(i[1]);
            }

            var pos3 = new List<int>();
            foreach (var i in _allBtnPosition)
            {
                pos3.Add(i[2]);
            }

            List<int> pos4 = _allBtnPosition[0];
            List<int> pos5 = _allBtnPosition[1];
            List<int> pos6 = _allBtnPosition[2];
            List<int> pos7 = new List<int>();
            List<int> pos8 = new List<int>();

            int i_ = 0;
            int j_ = 2;
            foreach(var i in _allBtnPosition)
            {
                pos7.Add(i[i_]);
                pos8.Add(i[j_]);
                i_++;
                j_--;
            }

            var allPos = new List<List<int>>();
            allPos.Add(pos1);
            allPos.Add(pos2);
            allPos.Add(pos3);
            allPos.Add(pos4);
            allPos.Add(pos5);
            allPos.Add(pos6);
            allPos.Add(pos7);
            allPos.Add(pos8);

            bool hasWon = false;
            foreach (var p in allPos)
            {
                int init = p[0];
                if(init != 2)
                {
                    var initLst = new List<int>();
                    foreach(var i in p)
                    {
                        if (i == init)
                        {
                            initLst.Add(i);
                        }
                    }
                    if (initLst.Count == 3)
                    {
                        hasWon = true;
                        break;
                    }
                }
            }

            return hasWon;
        }

        public void Single_Each_Btn_Clicked(object sender, EventArgs args)
        {
            if (_gameEnd == false)
            {
                var currentBtn = sender as Button;

                var columnStack = (StackLayout)currentBtn.Parent;
                int iColumn = 0;
                foreach (var btn in columnStack.Children)
                {
                    if (btn.Id == currentBtn.Id) { break; }
                    iColumn++;
                }

                var rowStack = (StackLayout)columnStack.Parent;
                int iRow = 0;

                foreach (var stack in rowStack.Children)
                {
                    if (stack.Id == columnStack.Id) { break; }
                    iRow++;
                }

                if (_allBtnPosition[iRow][iColumn] == 2)
                {
                    _allBtnPosition[iRow][iColumn] = 0;
                    currentBtn.FontSize = 30;
                    currentBtn.Text = "X";
                    currentBtn.BackgroundColor = Color.Transparent;

                    bool whoWon = Who_Won();
                    if (whoWon)
                    {
                        DisplaySingleGameStatus.Text = "You Won!";
                        _gameEnd = true;
                    }
                    else
                    {
                        var choosedByComputer = Choose_move();
                        _allBtnPosition[choosedByComputer[0]][choosedByComputer[1]] = 1;
                        var computerBtn = _allBtnObject[choosedByComputer[0]][choosedByComputer[1]];
                        computerBtn.BackgroundColor = Color.Transparent;
                        computerBtn.FontSize = 30;
                        computerBtn.Text = "O";

                        bool whoWonSecond = Who_Won();
                        if (whoWonSecond)
                        {
                            DisplaySingleGameStatus.Text = "Computer Won!";
                            _gameEnd = true;
                        }
                    }
                }
            }

        }

        public void Single_Reset_Clicked(object sender, EventArgs args)
        {
            App.Current.MainPage = new single_player();

        }
    }
}