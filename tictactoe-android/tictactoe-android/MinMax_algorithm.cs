﻿using System;

namespace tictactoe_android
{
    internal class Gfg
    {
        private class Move
		{
            public int Row { get; set; }
            public int Col { get; set; }
		};

        private const char Player = 'x';

        private const char Opponent = 'o';

        private static bool IsMovesLeft(char[,] board)
		{
			for (var i = 0; i < 3; i++)
				for (var j = 0; j < 3; j++)
					if (board[i, j] == '_')
						return true;
			return false;
		}

        private static int Evaluate(char[,] b)
		{
			for (var row = 0; row < 3; row++)
			{
				if (b[row, 0] == b[row, 1] &&
					b[row, 1] == b[row, 2])
				{
					if (b[row, 0] == Player)
						return +10;
                    else if (b[row, 0] == Opponent)
						return -10;
				}
			}

			for (var col = 0; col < 3; col++)
			{
				if (b[0, col] == b[1, col] &&
					b[1, col] == b[2, col])
				{
					if (b[0, col] == Player)
                        return +10;

					else if (b[0, col] == Opponent)
						return -10;
				}
			}

			if (b[0, 0] == b[1, 1] && b[1, 1] == b[2, 2])
			{
				if (b[0, 0] == Player)
					return +10;
				else if (b[0, 0] == Opponent)
					return -10;
			}

			if (b[0, 2] == b[1, 1] && b[1, 1] == b[2, 0])
			{
				if (b[0, 2] == Player)
					return +10;
				else if (b[0, 2] == Opponent)
					return -10;
			}

			return 0;
		}

        private static int Minimax(char[,] board, int depth, bool isMax)
		{
			int score = Evaluate(board);

			if (score == 10)
            {
                return score;
            }

            if (score == -10)
				return score;

			if (IsMovesLeft(board) == false)
				return 0;

			if (isMax)
			{
				int best = -1000;

				for (var i = 0; i < 3; i++)
				{
					for (var j = 0; j < 3; j++)
					{
						if (board[i, j] == '_')
						{
							board[i, j] = Player;

							best = Math.Max(best, Minimax(board,
											depth + 1, !isMax));

							board[i, j] = '_';
						}
					}
				}
				return best;
			}

			else
			{
				int best = 1000;

				for (var i = 0; i < 3; i++)
				{
					for (var j = 0; j < 3; j++)
					{
						if (board[i, j] == '_')
						{
							board[i, j] = Opponent;

							best = Math.Min(best, Minimax(board,
											depth + 1, !isMax));

							board[i, j] = '_';
						}
					}
				}
				return best;
			}
		}

        private static Move FindBestMove(char[,] board)
		{
			int bestVal = -1000;
			var bestMove = new Move();
			bestMove.Row = -1;
			bestMove.Col = -1;

			for (var i = 0; i < 3; i++)
			{
				for (var j = 0; j < 3; j++)
				{
					if (board[i, j] == '_')
					{
						board[i, j] = Player;

						var moveVal = Minimax(board, 0, false);

						board[i, j] = '_';

						if (moveVal > bestVal)
						{
							bestMove.Row = i;
							bestMove.Col = j;
							bestVal = moveVal;
						}
					}
				}
			}

			return bestMove;
		}
    }
}

